# Dotjector

A dotfile generation, templating and injection tool, based on the [tera](https://tera.netlify.app/) templating engine.

## Install
```bash
cargo install dotjector
```

## What is this?

Nowadays coding comes with using a lot of 3rd party tools that most of the times requires some sort of configuration dotfile in the root of the git repository. This does not only clutter the root dir, but most of the times also requires having generic information, such as the project version, copy pasted in a lof of dotfiles. This is especially troubeling when working with microservices which do life in seperate repositories, but still most of the time require the same tooling configurations.

## How are the problems solved?

Dotjector generates config files based on template and context files.

Template files are config files with missing data, for example the Cargo.toml of this repo could look like this:  
templates/Cargo.toml:
```
[package]
name = "{ project_config.name }"
version = "{ project_config.version }"
edition = "2018"
...
```

Context files provide the missing data:   
configs/project_config.yaml:
```
name: dotjector
version: 0.1.0  
```

Executing dotjector will then generate the file Cargo.toml with the interpolated values provided by the context files.

## Warning:

Dotjector was developed based on a idea that may or may not be best one. It might feels cumbersome to work with dotjector and may feels weird for people looking at a repo using it. So decide for your self whether it can be useful for your self, and if you find usecases that are worth noting make sure to get back to me. MRs are always welcome!

### Example:

A example can be found in a seperate example [repo](https://gitlab.com/manuel2258/dotjector-example).

### Project Status:
#### Features:
- [X] Basic functionallity
- [ ] Adhoc tasked based file generation
- [ ] Write tests

#### File fetch ways:
- [X] local
- [X] git
- [X] http

### File source formats:
- [X] plain text
- [X] .yaml
- [ ] json

#### DevOps / Integration:
- [X] Published as crate
- [/] Bundled and published as docker container (only dev image)
- [X] Pre-commit hook integration
- [/] Integrate CI and badges (no badges) 