use std::collections::HashMap;
use std::sync::Arc;

use tokio::sync::Mutex;

use crate::config::{Context, ContextEncoding, FileSource};
use crate::error;
use crate::source_store::SourceStore;

pub fn get_file_name_from_source(file_source: &FileSource) -> &str {
    match file_source {
        FileSource::local(file) => file,
        FileSource::git { file, .. } => file,
        FileSource::http(url) => url,
    }
}

pub async fn load(
    contexts: HashMap<String, Context>,
    source_store: Arc<Mutex<SourceStore>>,
) -> error::Result<tera::Context> {
    let mut context = tera::Context::new();

    for (root_name, file_context) in contexts {
        let file_content = match source_store.lock().await.get_content(&file_context.input) {
            Ok(f) => f,
            Err(_) => continue,
        };

        let value: serde_yaml::Value = match &file_context.encoding {
            ContextEncoding::plain => serde_yaml::Value::String(file_content),
            ContextEncoding::yaml => match serde_yaml::from_str(&file_content) {
                Ok(val) => val,
                Err(e) => {
                    return Err(error::Error::InputSourceDecodingError(
                        file_context.encoding,
                        get_file_name_from_source(&file_context.input).into(),
                        e.to_string(),
                    ))
                }
            },
        };

        context.insert(root_name, &value);
    }

    Ok(context)
}
