use thiserror::Error;

use crate::config::ContextEncoding;

pub type Result<T> = std::result::Result<T, Error>;

#[derive(Error, Debug)]
pub enum Error {
    #[error("Template with name {0} is not specified in config file")]
    TemplateNotFound(String),
    #[error("From template {0} required context {1} is not specified as context")]
    RequireContextNotFound(String, String),
    #[error("A template or context tries to use file source \"{0}\" although not loaded")]
    InputSourceNotFound(String),
    #[error("{0}")]
    FetchError(crate::fetch::Error),
    #[error("Could not parse input \"{1}\" as \"{0:?}\": {2}")]
    InputSourceDecodingError(ContextEncoding, String, String),
}
