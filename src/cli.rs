use clap::{AppSettings, Clap};

pub fn parse() -> DotjectorCli {
    DotjectorCli::parse()
}

#[derive(Clap)]
#[clap(version = env!("CARGO_PKG_VERSION"), author = "Manuel Schmidbauer <development@manu.software>")]
#[clap(setting = AppSettings::ColoredHelp)]
pub struct DotjectorCli {
    #[clap(
        short = 'c',
        long = "config",
        default_value = ".dotjector.yaml",
        about = "The to load dotjector configuration file"
    )]
    pub config: String,
    #[clap(short = 'l', long = "log-level", default_value = "3", possible_values = &["0", "1", "2", "3", "4"])]
    pub log_level: u8,
    #[clap(short = 'q', long = "quite")]
    pub quiet: bool,

    #[clap(subcommand)]
    pub subcmd: SubCommand,
}

#[derive(Clap)]
#[allow(non_camel_case_types)]
pub enum SubCommand {
    #[clap(about = "Generates output files from templates")]
    gen(GenerateSubcommand),
}

#[derive(Clap)]
#[clap(version = env!("CARGO_PKG_VERSION"), author = "Manuel Schmidbauer <development@manu.software>")]
#[clap(setting = AppSettings::ColoredHelp)]
pub struct GenerateSubcommand {
    #[clap(name = "TEMPLATE_NAME")]
    pub template_names: Vec<String>,
}
