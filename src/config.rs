use std::{collections::HashMap, fs::File};

use serde::{Deserialize, Serialize};

pub fn load(file_path: &str) -> Result<DotjectorConfig, ()> {
    let file = File::open(file_path).unwrap();
    let config: DotjectorConfig = serde_yaml::from_reader(file).unwrap();
    Ok(config)
}

#[derive(Serialize, Deserialize, Debug)]
pub struct DotjectorConfig {
    pub templates: HashMap<String, Template>,
    pub contexts: HashMap<String, Context>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Template {
    pub input: FileSource,
    pub output: String,
    pub requires: Option<Vec<String>>,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Context {
    pub input: FileSource,
    pub encoding: ContextEncoding,
}

#[derive(Serialize, Deserialize, Debug, Clone, Hash)]
#[allow(non_camel_case_types)]
pub enum ContextEncoding {
    plain,
    yaml,
}

#[derive(Serialize, Deserialize, Debug, Clone, Hash)]
#[allow(non_camel_case_types)]
pub enum FileSource {
    local(String),
    git {
        url: String,
        r#ref: String,
        file: String,
    },
    http(String),
}

impl std::fmt::Display for Template {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "Template({:?} -> {})", self.input, self.output)
    }
}
