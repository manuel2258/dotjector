use std::{fs::File, io::Write, sync::Arc};

use log::{info, warn};
use thiserror::Error;

use tera::Context;
use tokio::sync::Mutex;

use crate::{config::Template, source_store::SourceStore};

#[derive(Error, Debug)]
pub enum Error {
    #[error("Could not process template: {0:?}")]
    TeraError(tera::ErrorKind),
    #[error("Could not save output of template \"{0}\": {1}")]
    IoError(String, std::io::Error),
}

pub async fn process(
    context: Context,
    template_name: &str,
    template_config: &Template,
    source_store: Arc<Mutex<SourceStore>>,
) -> Result<(), Error> {
    info!("Processing template {:?}", &template_config);
    let input_content = source_store
        .lock()
        .await
        .get_content(&template_config.input)
        .unwrap();

    let output = match tera::Tera::one_off(&input_content, &context, false) {
        Ok(s) => s,
        Err(e) => return Err(Error::TeraError(e.kind)),
    };

    let mut output_file = match File::create(&template_config.output) {
        Ok(f) => f,
        Err(e) => return Err(Error::IoError(template_name.into(), e)),
    };

    output_file.write(output.as_bytes()).unwrap();
    warn!("Processed {}", &template_config);

    Ok(())
}
