use std::collections::hash_map::DefaultHasher;
use std::hash::{Hash, Hasher};
use std::io::Read;
use std::path::PathBuf;
use std::time::Instant;
use std::{fs::File, path::Path};

use app_dirs2::{get_app_dir, AppDataType, AppInfo};
use git2::Repository;
use log::{debug, error, info, warn};
use thiserror::Error;

use crate::config::FileSource;

const APP_INFO: AppInfo = AppInfo {
    name: "dotjector",
    author: "manu.software",
};

#[derive(Error, Debug)]
pub enum Error {
    #[error("Error while fetching file \"{0}\": {1}")]
    IoError(String, std::io::Error),
    #[error("{0}")]
    GitError(#[from] git2::Error),
    #[error("Could not find file \"{0}\" on git remote \"{1}\"")]
    GitFileNotFound(String, String),
    #[error("{0}")]
    ReqwestError(#[from] reqwest::Error),
}

type Result<T> = std::result::Result<T, Error>;

pub async fn fetch(config: FileSource) -> Result<String> {
    let res = match &config {
        FileSource::local(file) => fetch_local_file(&file),
        FileSource::git { url, r#ref, file } => fetch_git_file(&url, &r#ref, &file).await,
        FileSource::http(url) => fetch_http_file(&url).await,
    };

    warn!("Fetched input {:?}", &config);

    res
}

fn fetch_local_file(file_name: &str) -> Result<String> {
    read_file(file_name)
}

async fn fetch_git_file(git_url: &str, git_ref: &str, file_name: &str) -> Result<String> {
    let git_identifier = format!("{}@{}", git_url, git_ref);
    info!("Fetching {} from {}", file_name, git_identifier);
    let start_time = Instant::now();
    let mut git_cache_path_builder: PathBuf =
        get_app_dir(AppDataType::UserCache, &APP_INFO, "git").unwrap();

    let mut hasher = DefaultHasher::new();
    git_url.hash(&mut hasher);
    git_ref.hash(&mut hasher);
    let hash = hasher.finish();

    git_cache_path_builder.push(hash.to_string());
    let root_path: &Path = &git_cache_path_builder;
    let repo = if !root_path.exists() {
        info!(
            "First time using {}, cloning it into {}",
            &git_url,
            root_path.display(),
        );
        box_git_error(Repository::clone(git_url, root_path))?
    } else {
        debug!(
            "Using already cloned version of {} at {}",
            &git_url,
            root_path.display(),
        );

        box_git_error(Repository::open(root_path))?
    };
    assert!(root_path.exists());
    let mut remote = box_git_error(repo.find_remote("origin"))?;

    box_git_error(remote.fetch(&[git_ref], None, None))?;

    let full_ref = format!("refs/remotes/origin/{}", git_ref);
    box_git_error(repo.set_head(&full_ref))?;

    let current_head = box_git_error(repo.head())?;
    let current_commit = box_git_error(current_head.peel_to_commit())?;
    debug!(
        "Set local head to {}, now pointing at {}",
        full_ref,
        current_commit.id()
    );
    box_git_error(repo.reset(current_commit.as_object(), git2::ResetType::Hard, None))?;

    let id = match repo.index().unwrap().get_path(Path::new(file_name), 0) {
        Some(id) => id,
        None => return Err(Error::GitFileNotFound(file_name.into(), git_identifier)),
    }
    .id;

    let file_content = String::from_utf8(repo.find_blob(id).unwrap().content().into()).unwrap();
    debug!(
        "Took {}ms to fetch {} from git!",
        start_time.elapsed().as_millis(),
        file_name
    );
    Ok(file_content)
}

fn box_git_error<T>(input: std::result::Result<T, git2::Error>) -> Result<T> {
    match input {
        Ok(val) => Ok(val),
        Err(e) => return Err(Error::GitError(e)),
    }
}

async fn fetch_http_file(url: &str) -> Result<String> {
    debug!("Making http GET request to {}", url);

    let req = box_reqwest_error(reqwest::get(url).await)?;
    let resp = box_reqwest_error(req.text().await)?;

    Ok(resp)
}

fn box_reqwest_error<T>(input: std::result::Result<T, reqwest::Error>) -> Result<T> {
    match input {
        Ok(val) => Ok(val),
        Err(e) => return Err(Error::ReqwestError(e)),
    }
}

fn read_file(file_name: &str) -> Result<String> {
    let mut file = match File::open(file_name) {
        Ok(f) => f,
        Err(e) => return Err(Error::IoError(file_name.into(), e)),
    };
    let mut content = String::new();
    file.read_to_string(&mut content).unwrap();
    Ok(content)
}
