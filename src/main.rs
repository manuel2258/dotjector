mod cli;
mod config;
mod context;
mod error;
mod fetch;
mod process;
mod source_store;

use std::sync::Arc;

use cli::{GenerateSubcommand, SubCommand};
use colored::*;
use fern::{
    colors::{Color, ColoredLevelConfig},
    Dispatch,
};
use tokio::sync::Mutex;

fn setup_logger(log_level: u8) -> Result<(), fern::InitError> {
    let log_filter = match log_level {
        0 => log::LevelFilter::Trace,
        1 => log::LevelFilter::Debug,
        2 => log::LevelFilter::Info,
        3 => log::LevelFilter::Warn,
        4 => log::LevelFilter::Error,
        _ => panic!("invalid log level"),
    };

    let dispatch = Dispatch::new().level(log_filter).chain(std::io::stdout());

    let dispatch = if log_level < 3 {
        let colors = ColoredLevelConfig::new()
            .debug(Color::BrightBlack)
            .info(Color::White)
            .warn(Color::Yellow)
            .error(Color::Red);

        dispatch.format(move |out, message, record| {
            out.finish(format_args!(
                "{color_line}[{level}][{target}] {msg}",
                color_line =
                    format_args!("\x1B[{}m", colors.get_color(&record.level()).to_fg_str()),
                level = record.level(),
                target = record.target(),
                msg = message,
            ))
        })
    } else {
        let colors = ColoredLevelConfig::new()
            .warn(Color::White)
            .error(Color::Red);

        dispatch.format(move |out, message, record| {
            out.finish(format_args!(
                "{color_line} {msg}",
                color_line =
                    format_args!("\x1B[{}m", colors.get_color(&record.level()).to_fg_str()),
                msg = message,
            ))
        })
    };

    dispatch.apply()?;
    Ok(())
}

#[tokio::main()]
async fn main() {
    let cli_opts = cli::parse();

    if !cli_opts.quiet {
        setup_logger(cli_opts.log_level).unwrap();
    }

    let res = match cli_opts.subcmd {
        SubCommand::gen(gen) => generate_output(&gen).await,
    };

    match res {
        Ok(()) => (),
        Err(e) => {
            println!("{}", e.to_string().red());
            std::process::exit(-1);
        }
    }
}

async fn generate_output(cli_config: &GenerateSubcommand) -> error::Result<()> {
    let config = config::load(".dotjector.yaml").unwrap();

    let to_process_templates: Vec<&String> = if !cli_config.template_names.is_empty() {
        (&cli_config.template_names).iter().map(|k| k).collect()
    } else {
        config.templates.keys().collect()
    };

    let mut source_store = source_store::SourceStore::new(&config, to_process_templates.clone())?;
    source_store.wait_until_all_fetched().await?;
    let source_store_ptr = Arc::new(Mutex::new(source_store));

    let context = context::load(config.contexts, source_store_ptr.clone()).await?;

    let mut handles = Vec::new();

    for template_name in to_process_templates {
        let template = match config.templates.get(template_name) {
            Some(t) => t,
            None => return Err(error::Error::TemplateNotFound(template_name.into())),
        };
        handles.push(process::process(
            context.clone(),
            template_name,
            template,
            source_store_ptr.clone(),
        ))
    }

    let mut errored = false;

    for handle in handles {
        let res = handle.await;
        match res {
            Ok(()) => (),
            Err(e) => {
                println!("{}", e.to_string().red());
                errored = true;
            }
        }
    }

    if errored {
        std::process::exit(-1);
    }

    Ok(())
}
