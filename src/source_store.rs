use std::{
    collections::{hash_map::DefaultHasher, HashMap},
    hash::{Hash, Hasher},
};

use log::debug;
use tokio::task::{spawn, JoinHandle};

use crate::{config, fetch};
use crate::{config::FileSource, fetch::Error};
use crate::{context, error};

pub struct SourceStore {
    active_fetches: HashMap<u64, JoinHandle<Result<String, Error>>>,
    outputs: HashMap<u64, String>,
}

impl SourceStore {
    pub fn new(
        config: &config::DotjectorConfig,
        to_process_templates: Vec<&String>,
    ) -> error::Result<Self> {
        let mut source_store = Self {
            active_fetches: HashMap::new(),
            outputs: HashMap::new(),
        };

        for template_name in to_process_templates {
            let template = match config.templates.get(template_name) {
                Some(t) => t,
                None => return Err(error::Error::TemplateNotFound(template_name.into())),
            };
            source_store.add_to_fetch_source(template.input.clone());

            if template.requires.is_some() {
                for template_require in template.requires.as_ref().unwrap() {
                    let context_input = match config.contexts.get(template_require) {
                        Some(v) => v,
                        None => {
                            return Err(error::Error::RequireContextNotFound(
                                template_name.into(),
                                template_require.into(),
                            ))
                        }
                    };
                    source_store.add_to_fetch_source(context_input.input.clone());
                }
            }
        }

        Ok(source_store)
    }

    pub fn add_to_fetch_source(&mut self, source: FileSource) {
        let mut hasher = DefaultHasher::new();
        source.hash(&mut hasher);
        let hash = hasher.finish();

        if !self.active_fetches.contains_key(&hash) {
            debug!("Started fetch for {}::{:?}", &hash, &source);
            let handle = spawn(fetch::fetch(source));
            self.active_fetches.insert(hash, handle);
        } else {
            debug!("Got duplicate {}, will not refetch", &hash);
        }
    }

    pub async fn wait_until_all_fetched(&mut self) -> error::Result<()> {
        for (hash, handle) in self.active_fetches.drain() {
            let content = match handle.await.unwrap() {
                Ok(c) => c,
                Err(e) => return Err(error::Error::FetchError(e)),
            };
            self.outputs.insert(hash, content);
        }
        Ok(())
    }

    pub fn get_content(&self, source: &FileSource) -> error::Result<String> {
        let mut hasher = DefaultHasher::new();
        source.hash(&mut hasher);
        let hash = hasher.finish();

        match self.outputs.get(&hash) {
            Some(s) => Ok(s.into()),
            None => Err(error::Error::InputSourceNotFound(
                context::get_file_name_from_source(source).into(),
            )),
        }
    }
}
